Source: tkgate
Section: electronics
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>,
 Georges Khaznadar <georgesk@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (=13),
 tcl-dev,
 tk-dev,
 bison|byacc,
 flex,
 xutils-dev,
 libx11-dev,
 libpango1.0-dev
Homepage: http://www.tkgate.org/
Vcs-Browser: https://salsa.debian.org/science-team/tkgate.git
Vcs-Git: https://salsa.debian.org/science-team/tkgate.git

Package: tkgate
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, tkgate-data (= ${source:Version})
Recommends: tkgate-doc
Description: Tcl/Tk based digital circuit editor and simulator
 TkGate is a digital circuit editor and simulator with a Tcl/Tk based
 interface. TkGate includes a large number of built-in devices including basic
 gates, memories, ttys and modules for hierarchical design. The simulator can
 be controlled either interactively or through a simulation script. Memory
 contents can be loaded from files, and a microcode/macrocode compiler (gmac)
 is included to create tkgate memory files from a high-level description. The
 simulator supports continuous simulation, single step simulation (by clock or
 epoch) and breakpoints. Save files are in a Verilog-like format.
 .
 TkGate also includes a number of tutorial and example circuits which can be
 loaded through the "Help" menu. The examples range from a simple gate-level
 3-bit adder to a 16-bit CPU programmed to play the "Animals" game.
 .
 TkGate has a multi-language interface with support for English, Japanese,
 French and Spanish.

Package: tkgate-data
Architecture: all
Depends: ${misc:Depends}
Recommends: tkgate, tkgate-doc
Suggests: tcl, tk
Multi-Arch: foreign
Description: Tcl/Tk based digital circuit editor and simulator - data files
 TkGate is a digital circuit editor and simulator with a Tcl/Tk based
 interface.
 .
 This package contains the architecture independent data files.

Package: tkgate-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Tcl/Tk based digital circuit editor and simulator - documentation
 TkGate is a digital circuit editor and simulator with a Tcl/Tk based
 interface.
 .
 This package contains the documentation.
